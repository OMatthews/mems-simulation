# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 07:38:07 2020

@author: Oliver.Matthews
"""

from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt

def mems(y, t, K, k, c, omega, J):
    return [y[1], (K * np.cos(y[0]) * np.sin(2 * np.pi * omega * t) - k * y[0] - c * y[1]) / J]
    
    
# System constants
B = 1
l = 3e-3
k = 3e-5
Q = 20
J = 5e-13
A = l ** 2
r = l / 2

w_n = np.sqrt(k / J)
c = np.sqrt(k * J) / Q
print(f'Natural frequency is {w_n / (2* np.pi)}')

# Initialisation constants
theta_0 = 0
theta_dot_0 = 0

# Current constants
I0 = 0.1
omega = w_n / (2 * np.pi)

# Simulation constants
sim_time = 2e-2
resolution = 1e-6
delta_lim = 1e-8

# Wrap up the constants
K = 2 * r * l * B * I0


y0 = [theta_0, theta_dot_0]

times = np.arange(0, sim_time, resolution)

currents = I0 * np.sin(2 * np.pi * omega * times)

sol = odeint(mems, y0, times, args=(K, k, c, omega, J))

thetas = sol[:,0]
theta_dots = sol[:,1]

emfs = A * B * theta_dots * np.cos(thetas)


plt.figure(figsize = (6,8))
plt.subplot(3,1,1)
plt.plot(times, currents)
ax = plt.gca()
ax.set_xticklabels([])
plt.ylabel('Current (A)')

plt.subplot(3,1,2)
plt.plot(times, np.rad2deg(thetas), label = 'angle')
ax = plt.gca()
ax.set_xticklabels([])
plt.ylabel('Angle (degrees)')
ax = plt.gca()
yabs_max = abs(max(ax.get_ylim(), key=abs))
ax.set_ylim(ymin=-yabs_max, ymax=yabs_max)

plt.subplot(3,1,3)
plt.plot(times, emfs)
plt.ylabel('Back emf (V)')
plt.xlabel('Time (s)')
ax = plt.gca()
yabs_max = abs(max(ax.get_ylim(), key=abs))

ax.set_ylim(ymin=-yabs_max, ymax=yabs_max)


# plt.show()


# plt.plot(times, currents)
# plt.xlabel('Time (s)')
# plt.ylabel('Current (A)')

# ax = plt.gca()
# ax.twinx()
# plt.plot(times, emfs, 'r--')
# plt.ylabel('emf (V)')
