# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 20:48:42 2020

@author: Oliver.Matthews
"""
import numpy as np
import matplotlib.pyplot as plt

# System dynamics
def get_lorentz_force(B_field, i, l, theta):
    return B_field * i * 2 * l * np.cos(theta)

def get_torsional_torque(theta, k):
    return - theta * k

def get_damping_torque(theta_dot, c):
    return - theta_dot * c

def get_emf(B_field, A, theta_dot, theta):
    return B_field * A * theta_dot * np.cos(theta)


# Current characteristics
def get_current(I0, omega, time):
    return I0 * np.cos(omega * np.pi * time)

# System constants
B_field = 1
l = 3e-3
k = 6e-5
c = 2.5e-2
J = 5e-13
A = l ** 2
r = l / 2

# Initialisation constants
theta_0 = 0
theta_dot_0 = 0

# Current constants
I0 = 0.1
omega = 2e3

# Simulation constants
sim_time = 1e-2
resolution = 1e-6
delta_lim = 1e-8


# Initialisation
theta = theta_0
theta_dot = theta_dot_0
time = 0


# Initialise arrays for storage
thetas = []
emfs = []
currents = []
times = []
ang_vels = []

while time < sim_time:
    i = get_current(I0, omega, time)
    lorentz_force = get_lorentz_force(B_field, i, l, theta)
    torsional_torque = get_torsional_torque(theta, k)
    damping_torque = get_damping_torque(theta_dot, c)
    emf = get_emf(B_field, A, theta_dot, theta)

    total_torque = lorentz_force * r + torsional_torque + damping_torque
    theta_dot_dot = total_torque / J


    # Save values for the step
    thetas.append(theta)
    emfs.append(emf)
    currents.append(i)
    times.append(time)
    ang_vels.append(theta_dot)
    
    # Get next step. Make sure time step is not too big
    delta_theta_dot = theta_dot_dot * resolution
    if abs(delta_theta_dot) > delta_lim:
        delta_theta_dot = np.sign(delta_theta_dot) * delta_lim
        time_step = abs(delta_lim / theta_dot_dot)
    else:
        time_step = resolution
        
    time += time_step
    theta += (theta_dot + delta_theta_dot / 2) * time_step
    theta_dot += delta_theta_dot
    
plt.figure(figsize = (6,8))
plt.subplot(3,1,1)
plt.plot(times, currents)
ax = plt.gca()
ax.set_xticklabels([])
plt.ylabel('Current (A)')

plt.subplot(3,1,2)
plt.plot(times, np.rad2deg(thetas), label = 'angle')
ax = plt.gca()
ax.set_xticklabels([])
plt.ylabel('Angle (degrees)')
ax = plt.gca()
yabs_max = abs(max(ax.get_ylim(), key=abs))
ax.set_ylim(ymin=-yabs_max, ymax=yabs_max)

plt.subplot(3,1,3)
plt.plot(times, emfs)
plt.ylabel('Back emf (V)')
plt.xlabel('Time (s)')
ax = plt.gca()
yabs_max = abs(max(ax.get_ylim(), key=abs))

ax.set_ylim(ymin=-yabs_max, ymax=yabs_max)
